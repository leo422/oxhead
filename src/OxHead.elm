module OxHead exposing (Model, Msg, init, subscriptions, update, view)

import Browser
import Browser.Navigation as Nav
import Element exposing (Element, alignBottom, alignLeft, alignRight, alignTop, centerX, centerY, column, el, fill, fillPortion, height, image, link, maximum, newTabLink, onLeft, padding, paddingXY, px, rgb255, row, scrollbarY, spacing, text, width, wrappedRow)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import FormatNumber
import Html.Attributes
import Http exposing (get)
import Json.Decode exposing (index, int, list, map2, string)
import Json.Decode.Pipeline exposing (required)
import Svg exposing (circle, defs, g, polygon, polyline, rect, svg)
import Svg.Attributes as SvgA
import Time exposing (Posix)
import Url
import Url.Parser exposing (Parser, int, map, oneOf, parse, s, top)



-- Init


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( initialModel url key, Cmd.none )



-- Model


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , status : Status
    , nodeUrl : String
    , isMonitoring : Bool
    , nodeStats : Maybe NodeStats
    , showNodeStats : Bool
    , networkStatsList : Maybe (List NetworkStats)
    , showNetworkStats : Bool
    , nodeSettings : Maybe NodeSettings
    , showNodeSettings : Bool
    , utxoList : Maybe (List UTXO)
    , showUTXOList : Bool
    , tip : Maybe String
    , showTip : Bool
    , stakePoolsList : Maybe (List String)
    , showStakePoolsList : Bool
    , stakeDistribution : Maybe StakeDistribution
    , showStakeDistribution : Bool
    , showZeroStaked : Bool
    , sortAscending : Bool
    , leaders : Maybe (List Int)
    , showLeaders : Bool
    , leadersLogs : Maybe (List LeadersLogs)
    , showLeadersLogsList : Bool
    , fragmentLogs : Maybe (List FragmentLogs)
    , showFragmentLogsList : Bool
    , block : Maybe String
    , nextBlock : Maybe String
    , account : Maybe Account
    , blockId : String
    , accountId : String
    , nextBlockCount : Int
    , leaderId : String
    , error : String
    }


initialModel : Url.Url -> Nav.Key -> Model
initialModel url key =
    { url = url
    , key = key
    , status = Loaded
    , nodeUrl = "http://127.0.0.1:3100"
    , isMonitoring = False
    , nodeStats = Nothing
    , showNodeStats = True
    , showNetworkStats = False
    , networkStatsList = Nothing
    , nodeSettings = Nothing
    , showNodeSettings = True
    , utxoList = Nothing
    , tip = Nothing
    , showUTXOList = True
    , stakePoolsList = Nothing
    , stakeDistribution = Nothing
    , showTip = True
    , leaders = Nothing
    , leadersLogs = Nothing
    , showStakePoolsList = True
    , fragmentLogs = Nothing
    , block = Nothing
    , showStakeDistribution = True
    , showZeroStaked = False
    , sortAscending = False
    , nextBlock = Nothing
    , account = Nothing
    , showLeaders = True
    , blockId = ""
    , accountId = ""
    , showLeadersLogsList = True
    , nextBlockCount = 1
    , leaderId = ""
    , showFragmentLogsList = True
    , error = ""
    }



-- Navigation


type Route
    = Home
    | Node
    | Stakepools
    | Leaders
    | Settings
    | About
    | NotFound


route : Parser (Route -> a) a
route =
    oneOf
        [ map Home top
        , map Node (s "node")
        , map Stakepools (s "stakepools")
        , map Leaders (s "leaders")
        , map Settings (s "settings")
        , map About (s "about")
        ]


toRoute : String -> Route
toRoute string =
    case Url.fromString string of
        Nothing ->
            NotFound

        Just url ->
            Maybe.withDefault NotFound (parse route url)



-- Msg


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | FetchDataClicked
    | GotNodeStatsMessage (Result Http.Error NodeStats)
    | GotNetworkStatsListMessage (Result Http.Error (List NetworkStats))
    | GotNodeSettingsMessage (Result Http.Error NodeSettings)
    | GotUTXOListMessage (Result Http.Error (List UTXO))
    | GotTipMessage (Result Http.Error String)
    | GotStakePoolsListMessage (Result Http.Error (List String))
    | GotStakeDistributionMessage (Result Http.Error StakeDistribution)
    | GotLeadersMessage (Result Http.Error (List Int))
    | GotLeadersLogsListMessage (Result Http.Error (List LeadersLogs))
    | GotFragmentLogsListMessage (Result Http.Error (List FragmentLogs))
    | GotBlockMessage (Result Http.Error String)
    | GotNextBlockMessage (Result Http.Error String)
    | GotAccountMessage (Result Http.Error Account)
    | GotShutdownMessage (Result Http.Error String)
    | GotDeleteLeaderMessage (Result Http.Error String)
    | ToggleMonitoring
    | ToggleNodeStatsPanel
    | ToggleNodeSettingsPanel
    | ToggleNetworkStatsPanel
    | ToggleUTXOPanel
    | ToggleTipPanel
    | ToggleStakePoolsPanel
    | ToggleStakeDistributionPanel
    | ToggleLeadersPanel
    | ToggleLeadersLogsListPanel
    | ToggleFragmentLogsPanel
    | ToggleShowZeroStaked
    | ToggleSortAscending
    | Monitor Posix
    | NodeURLChanged String
    | BlockIdChanged String
    | NextBlockChanged String
    | AccountIdChanged String
    | LeaderIdChanged String
    | GetBlockClicked
    | GetNextBlockClicked
    | GetAccountClicked
    | DeleteLeaderClicked
    | GetShutdownClicked



-- Status


type Status
    = Loading
    | Loaded
    | Errored String



-- NetworkStats


type alias NetworkStats =
    { nodeId : String
    , establishedAt : String
    , lastBlockReceived : String
    , lastFragmentReceived : String
    , lastGossipReceived : String
    }


decodeNetworkStats : Json.Decode.Decoder NetworkStats
decodeNetworkStats =
    Json.Decode.succeed NetworkStats
        |> Json.Decode.Pipeline.required "nodeId" Json.Decode.string
        |> Json.Decode.Pipeline.required "establishedAt" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.optional "lastBlockReceived" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.optional "lastFragmentReceived" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.optional "lastGossipReceived" (Json.Decode.map formatDate Json.Decode.string) "null"


getNetworkStatsList : Model -> Cmd Msg
getNetworkStatsList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/network/stats"
        , expect = expectJson GotNetworkStatsListMessage (list decodeNetworkStats)
        }



-- NodeStats


type alias NodeStats =
    { blockRecvCnt : Int
    , lastBlockDate : String
    , lastBlockFees : Int
    , lastBlockHash : String
    , lastBlockHeight : String
    , lastBlockSum : Int
    , lastBlockTime : String
    , state : String
    , lastBlockTx : Int
    , txRecvCnt : Int
    , uptime : Int
    }


decodeNodeStats : Json.Decode.Decoder NodeStats
decodeNodeStats =
    Json.Decode.succeed NodeStats
        |> Json.Decode.Pipeline.required "blockRecvCnt" Json.Decode.int
        |> Json.Decode.Pipeline.required "lastBlockDate" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockFees" Json.Decode.int
        |> Json.Decode.Pipeline.required "lastBlockHash" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockHeight" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockSum" Json.Decode.int
        |> Json.Decode.Pipeline.optional "lastBlockTime" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.optional "state" Json.Decode.string "null"
        |> Json.Decode.Pipeline.required "lastBlockTx" Json.Decode.int
        |> Json.Decode.Pipeline.required "txRecvCnt" Json.Decode.int
        |> Json.Decode.Pipeline.required "uptime" Json.Decode.int


getNodeStats : Model -> Cmd Msg
getNodeStats model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/node/stats"
        , expect = expectJson GotNodeStatsMessage decodeNodeStats
        }



-- Settings


type alias NodeSettings =
    { block0Hash : String
    , block0Time : String
    , consensusVersion : String
    , currSlotStartTime : String
    , fees : NodeSettingsFees
    , maxTxsPerBlock : Int
    , slotDuration : Int
    , slotsPerEpoch : Int
    }


type alias NodeSettingsFees =
    { certificate : Int
    , coefficient : Int
    , constant : Int
    }


decodeNodeSettings : Json.Decode.Decoder NodeSettings
decodeNodeSettings =
    Json.Decode.succeed NodeSettings
        |> Json.Decode.Pipeline.required "block0Hash" Json.Decode.string
        |> Json.Decode.Pipeline.required "block0Time" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.required "consensusVersion" Json.Decode.string
        |> Json.Decode.Pipeline.optional "currSlotStartTime" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.required "fees" decodeNodeSettingsFees
        |> Json.Decode.Pipeline.required "maxTxsPerBlock" Json.Decode.int
        |> Json.Decode.Pipeline.required "slotDuration" Json.Decode.int
        |> Json.Decode.Pipeline.required "slotsPerEpoch" Json.Decode.int


decodeNodeSettingsFees : Json.Decode.Decoder NodeSettingsFees
decodeNodeSettingsFees =
    Json.Decode.succeed NodeSettingsFees
        |> Json.Decode.Pipeline.required "certificate" Json.Decode.int
        |> Json.Decode.Pipeline.required "coefficient" Json.Decode.int
        |> Json.Decode.Pipeline.required "constant" Json.Decode.int


getNodeSettings : Model -> Cmd Msg
getNodeSettings model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/settings"
        , expect = expectJson GotNodeSettingsMessage decodeNodeSettings
        }



-- UTXO


type alias UTXO =
    { transaction_id : String
    , index_in_transaction : Int
    , address : String
    , associated_fund : Int
    }


decodeUTXO : Json.Decode.Decoder UTXO
decodeUTXO =
    Json.Decode.succeed UTXO
        |> Json.Decode.Pipeline.required "transaction_id" Json.Decode.string
        |> Json.Decode.Pipeline.required "index_in_transaction" Json.Decode.int
        |> Json.Decode.Pipeline.required "address" Json.Decode.string
        |> Json.Decode.Pipeline.required "associated_fund" Json.Decode.int


getUTXOList : Model -> Cmd Msg
getUTXOList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/utxo"
        , expect = expectJson GotUTXOListMessage (list decodeUTXO)
        }



-- Tip


getTip : Model -> Cmd Msg
getTip model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/tip"
        , expect = Http.expectString GotTipMessage
        }



-- Stake Pools


getStakePools : Model -> Cmd Msg
getStakePools model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/stake_pools"
        , expect = Http.expectJson GotStakePoolsListMessage (Json.Decode.list Json.Decode.string)
        }



-- Stake Distribution


type alias StakeDistribution =
    { epoch : Int
    , stake : StakeDetail
    }


type alias StakeDetail =
    { dangling : Int
    , pools : List Pool
    , unassigned : Int
    }


type alias Pool =
    { id : String
    , value : Int
    }


decodeStakeDistribution : Json.Decode.Decoder StakeDistribution
decodeStakeDistribution =
    Json.Decode.succeed StakeDistribution
        |> Json.Decode.Pipeline.required "epoch" Json.Decode.int
        |> Json.Decode.Pipeline.required "stake" decodeStakeDetail


decodeStakeDetail : Json.Decode.Decoder StakeDetail
decodeStakeDetail =
    Json.Decode.succeed StakeDetail
        |> Json.Decode.Pipeline.required "dangling" Json.Decode.int
        |> Json.Decode.Pipeline.required "pools" (Json.Decode.list (Json.Decode.map2 Pool (index 0 Json.Decode.string) (index 1 Json.Decode.int)))
        |> Json.Decode.Pipeline.required "unassigned" Json.Decode.int


getStakeDistribution : Model -> Cmd Msg
getStakeDistribution model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/stake"
        , expect = expectJson GotStakeDistributionMessage decodeStakeDistribution
        }



-- Leaders


getLeaders : Model -> Cmd Msg
getLeaders model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/leaders"
        , expect = Http.expectJson GotLeadersMessage (Json.Decode.list Json.Decode.int)
        }



-- Leader Logs


type alias LeadersLogs =
    { created_at_time : String
    , scheduled_at_time : String
    , scheduled_at_date : String
    , wake_at_time : String
    , finished_at_time : String
    , status : LeaderLogsStatus
    , enclave_leader_id : Int
    }


type LeaderLogsStatus
    = Pending String
    | Rejected LeaderLogsStatusRejected
    | Created LeaderLogsStatusCreated


type alias LeaderLogsCreatedBlock =
    { block : String
    , chain_length : Int
    }


type alias LeaderLogsStatusCreated =
    { block : LeaderLogsCreatedBlock
    }


type alias LeaderLogsRejectedBlock =
    { reason : String
    }


type alias LeaderLogsStatusRejected =
    { rejected : LeaderLogsRejectedBlock
    }


decodeLeadersLogs : Json.Decode.Decoder LeadersLogs
decodeLeadersLogs =
    Json.Decode.succeed LeadersLogs
        |> Json.Decode.Pipeline.required "created_at_time" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.required "scheduled_at_time" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.required "scheduled_at_date" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.optional "wake_at_time" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.optional "finished_at_time" (Json.Decode.map formatDate Json.Decode.string) "null"
        |> Json.Decode.Pipeline.optional "status" (Json.Decode.oneOf [ Json.Decode.map Pending Json.Decode.string, Json.Decode.map Created decodeLeaderLogsStatusCreated, Json.Decode.map Rejected decodeLeaderLogsStatusRejected ]) (Pending "")
        |> Json.Decode.Pipeline.required "enclave_leader_id" Json.Decode.int


decodeLeaderLogsCreatedBlock : Json.Decode.Decoder LeaderLogsCreatedBlock
decodeLeaderLogsCreatedBlock =
    Json.Decode.succeed LeaderLogsCreatedBlock
        |> Json.Decode.Pipeline.required "block" Json.Decode.string
        |> Json.Decode.Pipeline.required "chain_length" Json.Decode.int


decodeLeaderLogsRejectedBlock : Json.Decode.Decoder LeaderLogsRejectedBlock
decodeLeaderLogsRejectedBlock =
    Json.Decode.succeed LeaderLogsRejectedBlock
        |> Json.Decode.Pipeline.required "reason" Json.Decode.string


decodeLeaderLogsStatusRejected : Json.Decode.Decoder LeaderLogsStatusRejected
decodeLeaderLogsStatusRejected =
    Json.Decode.succeed LeaderLogsStatusRejected
        |> Json.Decode.Pipeline.required "Rejected" decodeLeaderLogsRejectedBlock


decodeLeaderLogsStatusCreated : Json.Decode.Decoder LeaderLogsStatusCreated
decodeLeaderLogsStatusCreated =
    Json.Decode.succeed LeaderLogsStatusCreated
        |> Json.Decode.Pipeline.required "Block" decodeLeaderLogsCreatedBlock


getLeadersLogsList : Model -> Cmd Msg
getLeadersLogsList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/leaders/logs"
        , expect = expectJson GotLeadersLogsListMessage (list decodeLeadersLogs)
        }



-- Fragment Logs


type alias FragmentLogs =
    { fragment_id : String
    , received_from : String
    , received_at : String
    , last_updated_at : String
    , status : String
    }


decodeFragmentLogs : Json.Decode.Decoder FragmentLogs
decodeFragmentLogs =
    Json.Decode.succeed FragmentLogs
        |> Json.Decode.Pipeline.required "fragment_id" Json.Decode.string
        |> Json.Decode.Pipeline.required "received_from" Json.Decode.string
        |> Json.Decode.Pipeline.required "received_at" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.required "last_updated_at" (Json.Decode.map formatDate Json.Decode.string)
        |> Json.Decode.Pipeline.required "status" Json.Decode.string


getFragmentLogsList : Model -> Cmd Msg
getFragmentLogsList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/fragment/logs"
        , expect = expectJson GotFragmentLogsListMessage (list decodeFragmentLogs)
        }



-- Block


getBlock : Model -> Cmd Msg
getBlock model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/block/" ++ model.blockId
        , expect = Http.expectString GotBlockMessage
        }



-- Next Block


getNextBlock : Model -> Cmd Msg
getNextBlock model =
    Http.get
        { url =
            model.nodeUrl
                ++ "/api/v0/block/"
                ++ model.blockId
                ++ "/next_id"
                ++ (if model.nextBlockCount > 1 then
                        "?nextBlockCount=" ++ String.fromInt model.nextBlockCount

                    else
                        ""
                   )
        , expect = Http.expectString GotNextBlockMessage
        }



-- Account


type alias Account =
    { delegation : AccountDelegation
    , value : Int
    , counter : Int
    }


type alias AccountDelegation =
    { pools : List String
    }


decodeAccount : Json.Decode.Decoder Account
decodeAccount =
    Json.Decode.succeed Account
        |> Json.Decode.Pipeline.required "delegation" decodeAccountDelegation
        |> Json.Decode.Pipeline.required "value" Json.Decode.int
        |> Json.Decode.Pipeline.required "counter" Json.Decode.int


decodeAccountDelegation : Json.Decode.Decoder AccountDelegation
decodeAccountDelegation =
    Json.Decode.succeed AccountDelegation
        |> Json.Decode.Pipeline.required "pools" (Json.Decode.list Json.Decode.string)


getAccount : Model -> Cmd Msg
getAccount model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/account/" ++ model.accountId
        , expect = Http.expectJson GotAccountMessage decodeAccount
        }



-- Delete Leader


deleteLeader : Model -> Cmd Msg
deleteLeader model =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = model.nodeUrl ++ "/api/v0/leaders/" ++ model.leaderId
        , body = Http.emptyBody
        , expect = Http.expectString GotDeleteLeaderMessage
        , timeout = Nothing
        , tracker = Nothing
        }



-- Shutdown


getShutdown : Model -> Cmd Msg
getShutdown model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/shutdown"
        , expect = Http.expectString GotShutdownMessage
        }



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url }, Cmd.none )

        FetchDataClicked ->
            ( { model | status = Loading }
            , Cmd.batch <| List.map (\x -> x model) [ getNodeStats, getNetworkStatsList, getNodeSettings, getStakeDistribution, getLeadersLogsList ]
            )

        GetBlockClicked ->
            ( model
            , getBlock model
            )

        GetNextBlockClicked ->
            ( model
            , getNextBlock model
            )

        GetAccountClicked ->
            ( model
            , getAccount model
            )

        DeleteLeaderClicked ->
            ( model
            , deleteLeader model
            )

        GetShutdownClicked ->
            ( model
            , getShutdown model
            )

        ToggleMonitoring ->
            ( { model | isMonitoring = not model.isMonitoring }, Cmd.none )

        Monitor _ ->
            ( model, Cmd.batch <| List.map (\x -> x model) [ getNodeStats, getNetworkStatsList, getNodeSettings, getStakeDistribution, getLeadersLogsList ] )

        ToggleNodeStatsPanel ->
            ( { model | showNodeStats = not model.showNodeStats }, Cmd.none )

        ToggleNodeSettingsPanel ->
            ( { model | showNodeSettings = not model.showNodeSettings }, Cmd.none )

        ToggleNetworkStatsPanel ->
            ( { model | showNetworkStats = not model.showNetworkStats }, Cmd.none )

        ToggleUTXOPanel ->
            ( { model | showUTXOList = not model.showUTXOList }, Cmd.none )

        ToggleTipPanel ->
            ( { model | showTip = not model.showTip }, Cmd.none )

        ToggleStakePoolsPanel ->
            ( { model | showStakePoolsList = not model.showStakePoolsList }, Cmd.none )

        ToggleStakeDistributionPanel ->
            ( { model | showStakeDistribution = not model.showStakeDistribution }, Cmd.none )

        ToggleShowZeroStaked ->
            ( { model | showZeroStaked = not model.showZeroStaked }, Cmd.none )

        ToggleSortAscending ->
            ( { model | sortAscending = not model.sortAscending }, Cmd.none )

        ToggleLeadersPanel ->
            ( { model | showLeaders = not model.showLeaders }, Cmd.none )

        ToggleLeadersLogsListPanel ->
            ( { model | showLeadersLogsList = not model.showLeadersLogsList }, Cmd.none )

        ToggleFragmentLogsPanel ->
            ( { model | showFragmentLogsList = not model.showFragmentLogsList }, Cmd.none )

        GotNodeStatsMessage (Ok response) ->
            ( { model | status = Loaded, nodeStats = Just response }, Cmd.none )

        GotNodeStatsMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Node Status: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Node Status: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Node Status: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Node Status: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Node Status: " ++ err) }, Cmd.none )

        GotNetworkStatsListMessage (Ok response) ->
            ( { model | status = Loaded, networkStatsList = Just response }, Cmd.none )

        GotNetworkStatsListMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Network Status: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Network Status: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Network Status: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Network Status: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Network Status: " ++ err) }, Cmd.none )

        GotNodeSettingsMessage (Ok response) ->
            ( { model | status = Loaded, nodeSettings = Just response }, Cmd.none )

        GotNodeSettingsMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Node Settings: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Node Settings: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Node Settings: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Node Settings: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Node Settings: " ++ err) }, Cmd.none )

        GotUTXOListMessage (Ok response) ->
            ( { model | status = Loaded, utxoList = Just response }, Cmd.none )

        GotUTXOListMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve UTXO List: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve UTXO List: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve UTXO List: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve UTXO List: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve UTXO List: " ++ err) }, Cmd.none )

        GotTipMessage (Ok response) ->
            ( { model | status = Loaded, tip = Just response }, Cmd.none )

        GotTipMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ err) }, Cmd.none )

        GotStakePoolsListMessage (Ok response) ->
            ( { model | status = Loaded, stakePoolsList = Just response }, Cmd.none )

        GotStakePoolsListMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Pools: " ++ err) }, Cmd.none )

        GotStakeDistributionMessage (Ok response) ->
            ( { model | status = Loaded, stakeDistribution = Just response }, Cmd.none )

        GotStakeDistributionMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Distribution: " ++ url), error = url }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Distribution: " ++ "Timeout"), error = "Timeout" }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Distribution: " ++ "Network"), error = "Network" }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Distribution: " ++ String.fromInt statusCode), error = String.fromInt statusCode }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Stake Distribution: " ++ err), error = err }, Cmd.none )

        GotLeadersMessage (Ok response) ->
            ( { model | status = Loaded, leaders = Just response }, Cmd.none )

        GotLeadersMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders: " ++ err) }, Cmd.none )

        GotLeadersLogsListMessage (Ok response) ->
            ( { model | status = Loaded, leadersLogs = Just response }, Cmd.none )

        GotLeadersLogsListMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders Logs List: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders Logs List: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders Logs List: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders Logs List: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Leaders Logs List: " ++ err) }, Cmd.none )

        GotFragmentLogsListMessage (Ok response) ->
            ( { model | status = Loaded, fragmentLogs = Just response }, Cmd.none )

        GotFragmentLogsListMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Fragment Logs List: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Fragment Logs List: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Fragment Logs List: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Fragment Logs List: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Fragment Logs List: " ++ err) }, Cmd.none )

        GotBlockMessage (Ok response) ->
            ( { model | status = Loaded, block = Just response }, Cmd.none )

        GotBlockMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Block: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Block: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Block: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Block: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Block: " ++ err) }, Cmd.none )

        GotNextBlockMessage (Ok response) ->
            ( { model | status = Loaded, nextBlock = Just response }, Cmd.none )

        GotNextBlockMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Next Block: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Next Block: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Next Block: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Next Block: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Next Block: " ++ err) }, Cmd.none )

        GotAccountMessage (Ok response) ->
            ( { model | status = Loaded, account = Just response }, Cmd.none )

        GotAccountMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't retrieve Account: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't retrieve Account: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't retrieve Account: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't retrieve Account: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't retrieve Account: " ++ err) }, Cmd.none )

        GotDeleteLeaderMessage (Ok _) ->
            ( { model | status = Loaded }, Cmd.none )

        GotDeleteLeaderMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't Delete Leader: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't Delete Leader: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't Delete Leader: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't Delete Leader: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't Delete Leader: " ++ err) }, Cmd.none )

        GotShutdownMessage (Ok _) ->
            ( { model | status = Loaded }, Cmd.none )

        GotShutdownMessage (Err error) ->
            case error of
                Http.BadUrl url ->
                    ( { model | status = Errored ("Couldn't Shutdown Node: " ++ url) }, Cmd.none )

                Http.Timeout ->
                    ( { model | status = Errored ("Couldn't Shutdown Node: " ++ "Timeout") }, Cmd.none )

                Http.NetworkError ->
                    ( { model | status = Errored ("Couldn't Shutdown Node: " ++ "Network") }, Cmd.none )

                Http.BadStatus statusCode ->
                    ( { model | status = Errored ("Couldn't Shutdown Node: " ++ String.fromInt statusCode) }, Cmd.none )

                Http.BadBody err ->
                    ( { model | status = Errored ("Couldn't Shutdown Node: " ++ err) }, Cmd.none )

        NodeURLChanged url ->
            ( { model | nodeUrl = url }, Cmd.none )

        BlockIdChanged id ->
            ( { model | blockId = id }, Cmd.none )

        AccountIdChanged id ->
            ( { model | accountId = id }, Cmd.none )

        LeaderIdChanged id ->
            ( { model | leaderId = id }, Cmd.none )

        NextBlockChanged count ->
            ( { model
                | nextBlockCount =
                    case String.toInt count of
                        Just int ->
                            int

                        Nothing ->
                            1
              }
            , Cmd.none
            )


expectJson : (Result Http.Error a -> msg) -> Json.Decode.Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (Http.BadUrl url)

                Http.Timeout_ ->
                    Err Http.Timeout

                Http.NetworkError_ ->
                    Err Http.NetworkError

                Http.BadStatus_ metadata _ ->
                    Err (Http.BadStatus metadata.statusCode)

                Http.GoodStatus_ _ body ->
                    case Json.Decode.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err (Http.BadBody (Json.Decode.errorToString err))



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.isMonitoring then
        Time.every 1000 Monitor

    else
        Sub.none



-- View


adaIcon : Element Msg
adaIcon =
    el [ width (px 12), centerY ] <| Element.html <| adaSvg


toggleIcon : Bool -> Element Msg
toggleIcon open =
    if open then
        el [] <| Element.html <| anchorSvg

    else
        el [ Element.rotate (degrees 180) ] <| Element.html <| anchorSvg


toggleShowHideIcon : Bool -> Element Msg
toggleShowHideIcon show =
    if show then
        el [] <| Element.html <| showSvg

    else
        el [] <| Element.html <| hideSvg


toggleSortingIcon : Bool -> Element Msg
toggleSortingIcon asc =
    if asc then
        el [] <| Element.html <| sortAscSvg

    else
        el [] <| Element.html <| sortDscSvg


view : Model -> Browser.Document Msg
view model =
    { title = "Ox Head"
    , body =
        [ Element.layout
            [ Background.color mainBackgroundColor ]
          <|
            row [ height fill, width fill ]
                [ viewSidebar model
                , column [ width fill, height fill, scrollbarY ]
                    [ column [ width fill, height fill, paddingXY 10 0, spacing 10 ]
                        [ -- , row []
                          --     [ Input.text [ centerY, width (px 300) ] { onChange = NodeURLChanged, text = model.nodeUrl, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Node URL")), label = Input.labelLeft [ centerY ] (text "Node URL: ") }
                          --     ]
                          --   row [ width fill, spacing 10 ]
                          --     [ Input.button [] { onPress = Just GetStatsClicked, label = text "Get Stats" }
                          --     , Input.button []
                          --         { onPress = Just ToggleMonitoring
                          --         , label =
                          --             if model.isMonitoring then
                          --                 image [ width (px 30) ] { src = "./assets/loaded.svg", description = "Load Completed" }
                          --             else
                          --                 image [ width (px 30) ] { src = "./assets/errored.svg", description = "Load Completed" }
                          --         }
                          --     ]
                          -- , row []
                          --     [ Input.text [ centerY, width (px 300) ] { onChange = BlockIdChanged, text = model.blockId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Block ID")), label = Input.labelLeft [ centerY ] (text "Block ID: ") }
                          --     , Input.button buttonStyle { onPress = Just GetBlockClicked, label = text "Get Block" }
                          --     , Input.text [ centerY, width (px 50) ] { onChange = NextBlockChanged, text = String.fromInt model.nextBlockCount, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Next Block Count")), label = Input.labelLeft [ centerY ] (text "Count: ") }
                          --     , Input.button buttonStyle { onPress = Just GetNextBlockClicked, label = text "Get Next Block" }
                          --     ]
                          -- , row []
                          --     [ Input.text [ centerY, width (px 300) ] { onChange = AccountIdChanged, text = model.accountId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Account ID")), label = Input.labelLeft [ centerY ] (text "Account ID: ") }
                          --     , Input.button buttonStyle { onPress = Just GetAccountClicked, label = text "Get Account" }
                          --     ]
                          -- , row []
                          --     [ Input.text [ centerY, width (px 300) ] { onChange = LeaderIdChanged, text = model.leaderId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Leader ID")), label = Input.labelLeft [ centerY ] (text "Leader ID: ") }
                          --     , Input.button buttonStyle { onPress = Just DeleteLeaderClicked, label = text "Delete Leader" }
                          --     ]
                          -- , row []
                          --     [ Input.button buttonStyle { onPress = Just GetShutdownClicked, label = text "Shutdown" }
                          --     ]
                          -- , column []
                          --     [ case model.block of
                          --         Just block ->
                          --             el [] <| text block
                          --         Nothing ->
                          --             Element.none
                          --     , case model.nextBlock of
                          --         Just block ->
                          --             el [] <| text block
                          --         Nothing ->
                          --             Element.none
                          --     ]
                          case toRoute (Url.toString model.url) of
                            Home ->
                                Element.none

                            Node ->
                                wrappedRow
                                    [ Font.size 16
                                    , fontMono
                                    , spacing 15
                                    , padding 10
                                    , width fill
                                    ]
                                    [ case model.nodeStats of
                                        Just stats ->
                                            viewPanel (viewNodeStats stats model.showNodeStats)

                                        Nothing ->
                                            Element.none
                                    , case model.nodeSettings of
                                        Just stats ->
                                            viewPanel (viewNodeSettings stats model.showNodeSettings)

                                        Nothing ->
                                            Element.none
                                    , case model.networkStatsList of
                                        Just networkStatsData ->
                                            viewPanel (viewNetworkStats networkStatsData model.showNetworkStats)

                                        Nothing ->
                                            Element.none

                                    -- , case model.utxoList of
                                    --     Just stats ->
                                    --         viewPanel (viewUTXOList stats)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.tip of
                                    --     Just tip ->
                                    --         el [] <| text tip
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.stakePoolsList of
                                    --     Just stakePoolsList ->
                                    --         viewPanel (viewStakePools stakePoolsList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.stakeDistribution of
                                    --     Just stakeDistribution ->
                                    --         viewPanel (viewStakeDistribution stakeDistribution)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.leaders of
                                    --     Just leadersList ->
                                    --         viewPanel (viewLeaders leadersList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.leadersLogs of
                                    --     Just leadersLogsList ->
                                    --         viewPanel (viewLeadersLogs leadersLogsList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.fragmentLogs of
                                    --     Just fragmentLogsList ->
                                    --         viewPanel (viewFragmentLogs fragmentLogsList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.account of
                                    --     Just acc ->
                                    --         viewPanel (viewAccount acc)
                                    --     Nothing ->
                                    --         Element.none
                                    ]

                            Stakepools ->
                                row
                                    [ Font.size 16
                                    , fontMono
                                    , spacing 15
                                    , padding 10
                                    , width fill
                                    ]
                                    -- [ case model.stakePoolsList of
                                    --     Just stakePoolsList ->
                                    --         viewPanel (viewStakePools stakePoolsList model.showStakePoolsList)
                                    --     Nothing ->
                                    --         Element.none
                                    [ case model.stakeDistribution of
                                        Just stakeDistribution ->
                                            column [ centerX, width fill ] (viewStakeDistribution stakeDistribution model.showStakeDistribution model.showZeroStaked model.sortAscending)

                                        Nothing ->
                                            Element.none

                                    -- , case model.leaders of
                                    --     Just leadersList ->
                                    --         viewPanel (viewLeaders leadersList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.leadersLogs of
                                    --     Just leadersLogsList ->
                                    --         viewPanel (viewLeadersLogs leadersLogsList)
                                    --     Nothing ->
                                    --         Element.none
                                    -- , case model.fragmentLogs of
                                    --     Just fragmentLogsList ->
                                    --         viewPanel (viewFragmentLogs fragmentLogsList)
                                    --     Nothing ->
                                    --         Element.none
                                    ]

                            Leaders ->
                                case model.leadersLogs of
                                    Just leadersLogsList ->
                                        viewLeadersLogs leadersLogsList

                                    Nothing ->
                                        Element.none

                            Settings ->
                                column [ spacing 20, padding 20 ]
                                    [ row [ spacing 20, padding 20 ]
                                        [ Input.text [ centerY, width (px 300), fontMono, spacing 20 ] { onChange = NodeURLChanged, text = model.nodeUrl, placeholder = Maybe.Just (Input.placeholder [ centerY, fontMono ] (text "Insert Node URL")), label = Input.labelAbove [ centerY, fontNormal ] (text "Set your Node REST API Listening Address:") }
                                        ]
                                    , column [ spacing 20, padding 20 ]
                                        [ el [] (text "In order to work, this domain should be addded to the CORS section of your node configuration: ")
                                        , el [ fontMono, Font.size 12 ] (text " \"rest\": { \"listen\": \"127.0.0.1:3100\", \"cors\": { \"allowed_origins\": [\"https://oxhead.cowo.dev\"]}}")
                                        , el [] (text "You may also require to allow mixed contents on your browser")
                                        , image
                                            []
                                            { src = "./assets/disableProtection.png", description = "Disable Protection" }
                                        ]
                                    ]

                            About ->
                                column [ centerX, centerY, spacing 10, padding 10 ]
                                    [ el [ Font.color (Element.rgb 0 0 0), centerX, width (px 60), Font.center] (Element.html <| oxheadSvg)
                                    , el [ centerX, Region.heading 1, Font.size 36, fontMono, Font.center] (text "OXHEAD")
                                    , el [ centerX, Region.heading 2, Font.size 28, fontMono ] (text "Dashboard and control system for your Cardano node")
                                    , row [ centerX, spacing 2 ]
                                        [ el [ centerX, fontMono ] (text "Made with Elm and <3 by ")
                                        , newTabLink [ Font.color blue, fontMono ]
                                            { url = "https://twitter.com/arguser"
                                            , label = text "Leo"
                                            }
                                        ]
                                    , row [ centerX, spacing 5, padding 20 ]
                                        [ newTabLink [ Font.color blue, fontMono ]
                                            { url = "https://www.cardano.org/en/home/"
                                            , label = text "Cardano"
                                            }
                                        , newTabLink [ Font.color blue, fontMono ]
                                            { url = "https://github.com/input-output-hk/jormungandr"
                                            , label = text "Jörmungandr"
                                            }
                                        ]
                                    , row [ centerX, spacing 5, padding 20 ]
                                        [ el [] (text "Hosted on")
                                        , newTabLink [ Font.color blue, fontMono ]
                                            { url = "https://gitlab.com/arguser/oxhead"
                                            , label = text "GitLab"
                                            }
                                        ]
                                    , column [ alignBottom, centerX, spacing 5, padding 20 ]
                                        [ el [ centerX ] (text "Support")
                                        , newTabLink [ Font.color blue, fontMono ]
                                            { url = "https://seiza.com/blockchain/address/Ae2tdPwUPEZHc31dY71ehfqF69JDkgi9CN3Ka2rQi9ZPsKiTyYbeZ7MYyZc"
                                            , label = row [ spacing 5 ] [ adaIcon, text "Ae2tdPwUPEZHc31dY71ehfqF69JDkgi9CN3Ka2rQi9ZPsKiTyYbeZ7MYyZc" ]
                                            }
                                        ]
                                    ]

                            _ ->
                                Element.none
                        ]
                    ]
                ]
        ]
    }


viewSidebar : Model -> Element Msg
viewSidebar model =
    column [ height fill, Background.color sidebarBackgroundColor, spacing 10, padding 10, centerX ]
        [ row [ padding 20, alignTop ]
            [ el [ centerX, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ] ]
                (link []
                    { url = "/"
                    , label =
                        withTooltip "Home"
                            (Element.html <| oxheadSvg)
                    }
                )
            ]
        , column [ centerX, spacing 40 ]
            [ el [ width (px 40), centerX, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ] ] (Input.button [] { onPress = Just FetchDataClicked, label = withTooltip "Fetch Data" <| Element.html <| fetchDataSvg })
            , Input.button [ width (px 40), centerX, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ] ]
                { onPress = Just ToggleMonitoring
                , label =
                    if model.isMonitoring then
                        withTooltip "Stop Monitoring" <| Element.html <| monitorSvg

                    else
                        withTooltip "Start Monitoring" <| Element.html <| monitorSvg
                }
            , el
                [ alignBottom
                , centerX
                , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
                , Font.color
                    (if toRoute (Url.toString model.url) == Node then
                        Element.rgb255 255 255 255

                     else
                        Element.rgb255 94 96 102
                    )
                ]
                (link [ centerX ]
                    { url = "/node"
                    , label = withTooltip "Node" <| Element.html <| nodeSvg
                    }
                )
            , el
                [ alignBottom
                , centerX
                , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
                , Font.color
                    (if toRoute (Url.toString model.url) == Stakepools then
                        Element.rgb255 255 255 255

                     else
                        Element.rgb255 94 96 102
                    )
                ]
                (link [ centerX ]
                    { url = "/stakepools"
                    , label = withTooltip "Stake Pools" <| Element.html <| stakePoolsSvg
                    }
                )
            , el
                [ alignBottom
                , centerX
                , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
                , Font.color
                    (if toRoute (Url.toString model.url) == Leaders then
                        Element.rgb255 255 255 255

                     else
                        Element.rgb255 94 96 102
                    )
                ]
                (link [ centerX ]
                    { url = "/leaders"
                    , label = withTooltip "Leaders" <| Element.html <| leadersSvg
                    }
                )
            ]
        , column [ Region.navigation, centerX, spacing 20, alignBottom, paddingXY 0 40 ] (bottomNavElements model)
        , column
            [ Region.footer
            , width fill
            , alignBottom
            , Background.color sidebarBackgroundColor
            , height (px 40)
            , paddingXY 10 0
            , centerX
            ]
            [ el [ centerX, width (px 40) ] <|
                case model.status of
                    Loading ->
                        Element.html <| loadingSvg

                    Loaded ->
                        Element.html <| loadedSvg

                    Errored _ ->
                        Element.html <| errorerSvg
            ]
        ]


bottomNavElements : Model -> List (Element Msg)
bottomNavElements model =
    [ el
        [ alignBottom
        , centerX
        , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == Settings then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        ]
        (link []
            { url = "/settings"
            , label = withTooltip "Settings" (Element.html <| settingsSvg)
            }
        )
    , el
        [ alignBottom
        , centerX
        , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == About then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        ]
        (link []
            { url = "/about"
            , label = withTooltip "About" (Element.html <| aboutSvg)
            }
        )
    ]


viewPanel : List (Element Msg) -> Element Msg
viewPanel viewContent =
    column
        [ Border.color (Element.rgb255 198 205 214)
        , Border.width 2
        , padding 10
        , spacing 5
        , alignTop
        , width fill
        ]
        viewContent


viewNodeStats : NodeStats -> Bool -> List (Element Msg)
viewNodeStats nodeStats showNodeStats =
    [ row [ width fill, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, paddingXY 2 10 ]
        [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Node Status"
        , Input.button [ alignRight ] { onPress = Just ToggleNodeStatsPanel, label = toggleIcon showNodeStats }
        ]
    , if showNodeStats then
        column [ spacing 10, padding 10 ]
            [ row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Number of blocks received by node" <| text <| "Blocks Recieved")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.blockRecvCnt
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "The Epoch and slot Number of the block" <| text <| "Last Block Date")
                , el [ alignRight ] <| text <| nodeStats.lastBlockDate
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Sum of all fee values in all transactions in last block" <| text <| "Last Block Fees")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockFees
                ]
            , row [ width fill, spacing 5 ]
                [ el [ alignLeft ] (withTooltip "The block hash, it's unique identifier in the blockchain" <| text <| "Last Block Hash")
                , el [ alignRight ] <| text <| nodeStats.lastBlockHash
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "The block number, in order, since the block0" <| text <| "Last Block Height")
                , el [ alignRight ] <| text <| nodeStats.lastBlockHeight
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Sum of all input values in all transactions in last block" <| text <| "Last Block Sum")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockSum
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "When last block was created, not set if none was created yet" <| text <| "Last Block Time")
                , el [ alignRight ] <| text <| nodeStats.lastBlockTime
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Number of transactions in last block" <| text <| "Last Block Tx")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockTx
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "State of the node" <| text <| "State")
                , el [ alignRight ] <| text <| nodeStats.state
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Number of transactions received by node" <| text <| "TX Received Count")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.txRecvCnt
                ]
            , row [ width fill ]
                [ el [ alignLeft ] (withTooltip "Node uptime in seconds" <| text <| "Uptime")
                , el [ alignRight ] <| text <| String.fromInt nodeStats.uptime
                ]
            ]

      else
        Element.none
    ]


viewNetworkStats : List NetworkStats -> Bool -> List (Element Msg)
viewNetworkStats networkStats showNetworkStats =
    [ column [ width fill, spacing 10 ]
        [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Network Status"
            , Input.button toggleButtonStyle { onPress = Just ToggleNetworkStatsPanel, label = toggleIcon showNetworkStats }
            ]
        , row [ fontNormal, Font.size 16, width fill, Font.bold ]
            [ el [ alignLeft ] <| text <| "Connections"
            , el [ alignRight ] <| text <| String.fromInt (List.length networkStats)
            ]
        ]
    , if showNetworkStats then
        column
            [ width fill
            , spacing 10
            , padding 10
            , scrollbarY
            , height (fill |> maximum 400)
            ]
            (List.map
                (\stats ->
                    column [ width fill, padding 20, Background.color (Element.rgb255 240 241 242), Border.solid, Border.color (Element.rgb255 198 205 214), Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 } ]
                        [ row [ width fill ]
                            [ withTooltip "Hex-encoded node ID" <| el [ alignLeft ] <| text <| "Node ID"
                            , el [ alignRight ] <| text <| stats.nodeId
                            ]
                        , row [ width fill ]
                            [ withTooltip "Timestamp from when the connection was established at" <| el [ alignLeft ] <| text <| "Established At"
                            , el [ alignRight ] <| text <| stats.establishedAt
                            ]
                        , row [ width fill ]
                            [ withTooltip "Timestamp of last time block was received from node if ever" <| el [ alignLeft ] <| text <| "Last Block Received"
                            , el [ alignRight ] <| text <| stats.lastBlockReceived
                            ]
                        , row [ width fill ]
                            [ withTooltip "Timestamp of last time fragment was received from node if ever" <| el [ alignLeft ] <| text <| "Last Fragment Received"
                            , el [ alignRight ] <| text <| stats.lastFragmentReceived
                            ]
                        , row [ width fill ]
                            [ withTooltip "Timestamp of last time gossip was received from node if ever" <| el [ alignLeft ] <| text <| "Last Fragment Received"
                            , el [ alignRight ] <| text <| stats.lastGossipReceived
                            ]
                        ]
                )
                networkStats
            )

      else
        Element.none
    ]


viewNodeSettings : NodeSettings -> Bool -> List (Element Msg)
viewNodeSettings nodeSettings showNodeSettings =
    [ row [ width fill, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, paddingXY 2 10 ]
        [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Node Settings"
        , Input.button toggleButtonStyle { onPress = Just ToggleNodeSettingsPanel, label = toggleIcon showNodeSettings }
        ]
    , if showNodeSettings then
        column [ width fill, spacing 10, padding 10 ]
            [ row [ width fill ]
                [ withTooltip "Hex-encoded hash of block0" <| el [ alignLeft ] <| text <| "Blocks Hash"
                , el [ alignRight ] <| text <| nodeSettings.block0Hash
                ]
            , row [ width fill ]
                [ withTooltip "When block0 was created" <| el [ alignLeft ] <| text <| "Block Time"
                , el [ alignRight ] <| text <| nodeSettings.block0Time
                ]
            , row [ width fill ]
                [ withTooltip "Version of consensus, which is currently used" <| el [ alignLeft ] <| text <| "Consensus Version"
                , el [ alignRight ] <| text <| nodeSettings.consensusVersion
                ]
            , row [ width fill ]
                [ withTooltip "When current slot was opened, not set if none is currently open" <| el [ alignLeft ] <| text <| "Current Slot Start Time"
                , el [ alignRight ] <| text <| nodeSettings.currSlotStartTime
                ]
            , row [ width fill ]
                [ viewPanel (viewNodeSettingsFees nodeSettings.fees)
                ]
            , row [ width fill ]
                [ withTooltip "Maximum number of transactions in block" <| el [ alignLeft ] <| text <| "Max Tx Per Block"
                , el [ alignRight ] <| text <| String.fromInt nodeSettings.maxTxsPerBlock
                ]
            , row [ width fill ]
                [ withTooltip "Slot duration in seconds" <| el [ alignLeft ] <| text <| "Slot Duration"
                , el [ alignRight ] <| text <| String.fromInt nodeSettings.slotDuration
                ]
            , row [ width fill ]
                [ withTooltip "Number of slots per epoch" <| el [ alignLeft ] <| text <| "Slots Per Epoch"
                , el [ alignRight ] <| text <| String.fromInt nodeSettings.slotsPerEpoch
                ]
            ]

      else
        Element.none
    ]


viewNodeSettingsFees : NodeSettingsFees -> List (Element Msg)
viewNodeSettingsFees nodeSettingsFees =
    [ row [ width fill ]
        [ withTooltip "Fee per certificate used in witness" <| el [ alignLeft ] <| text <| "Certificate"
        , el [ alignRight ] <| text <| String.fromInt nodeSettingsFees.certificate
        ]
    , row [ width fill ]
        [ withTooltip "Fee per every input and output of transaction" <| el [ alignLeft ] <| text <| "Coefficient"
        , el [ alignRight ] <| text <| String.fromInt nodeSettingsFees.coefficient
        ]
    , row [ width fill ]
        [ withTooltip "Base fee per transaction" <| el [ alignLeft ] <| text <| "Constant"
        , el [ alignRight ] <| text <| String.fromInt nodeSettingsFees.constant
        ]
    ]


viewUTXOList : List UTXO -> List (Element Msg)
viewUTXOList utxoList =
    List.map
        (\utxo ->
            column []
                [ row []
                    [ el [ alignLeft ] <| text <| "Transaction ID"
                    , el [ alignRight ] <| text <| utxo.transaction_id
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Index In Transaction ID"
                    , el [ alignRight ] <| text <| String.fromInt utxo.index_in_transaction
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Address"
                    , el [ alignRight ] <| text <| utxo.address
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Associated Fund"
                    , el [ alignRight ] <| text <| String.fromInt utxo.associated_fund
                    ]
                ]
        )
        utxoList


viewStakePools : List String -> Bool -> List (Element Msg)
viewStakePools stakePoolsList showStakePools =
    [ column [ width fill, spacing 10 ]
        [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Stake Pools List"
            , Input.button toggleButtonStyle { onPress = Just ToggleStakePoolsPanel, label = toggleIcon showStakePools }
            ]
        , row [ fontNormal, Font.size 16, width fill ]
            [ el [ alignLeft ] <| text <| "Total Stake Pools"
            , el [ alignRight ] <| text <| String.fromInt (List.length stakePoolsList)
            ]
        ]
    , if showStakePools then
        column [ width fill, spacing 10, padding 10 ]
            (viewStakePoolsSimple stakePoolsList)

      else
        Element.none
    ]


viewStakePoolsSimple : List String -> List (Element Msg)
viewStakePoolsSimple stakePoolsList =
    List.map
        (\stakePool ->
            el [ spacing 5 ] <| text <| stakePool
        )
        stakePoolsList


viewStakeDistribution : StakeDistribution -> Bool -> Bool -> Bool -> List (Element Msg)
viewStakeDistribution stakeDistribution showStakeDistribution showZeroStaked sortDescending =
    [ column [ width fill, spacing 10 ]
        [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 32 ] <| text <| "Stake Distribution"
            ]
        ]
    , row [ width fill, padding 10, spacing 30 ]
        [ column [ width fill, spacing 10, padding 10, alignTop, fontMono, Border.widthEach { bottom = 0, left = 0, right = 1, top = 0 }, Font.size 18 ]
            [ row [ width fill ]
                [ el [ alignLeft ] <| text <| "Stake Pools Staking"
                , el [ alignRight ] <| text <| String.fromInt (List.length (List.filter (\stakePool -> stakePool.value > 0) stakeDistribution.stake.pools))
                ]
            , row [ width fill ]
                [ el [ alignLeft ] <| text <| "Total Stake Pools"
                , el [ alignRight ] <| text <| String.fromInt (List.length stakeDistribution.stake.pools)
                ]
            , if showStakeDistribution then
                column [ width fill, spacing 10 ]
                    [ row [ width fill ]
                        [ withTooltip "Epoch of last block" <| el [ alignLeft ] <| text <| "Epoch"
                        , el [ alignRight ] <| text <| String.fromInt stakeDistribution.epoch
                        ]
                    , column [ width fill, spacing 10 ] (viewStakeDetail stakeDistribution.stake)
                    ]

              else
                Element.none
            ]
        , viewStakePoolsTable stakeDistribution.stake.pools showZeroStaked sortDescending
        ]
    ]


viewStakeDetail : StakeDetail -> List (Element Msg)
viewStakeDetail stakeDetail =
    [ row [ width fill ]
        [ withTooltip "Total value stored in accounts, but assigned to nonexistent pools" <| el [ alignLeft ] <| text <| "Dangling"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| String.fromInt stakeDetail.dangling ]
        ]

    -- , row [ width fill ]
    --     [ el [ alignLeft ] <| text <| "Pools"
    --     , viewPanel (viewStakePoolsDetails stakeDetail.pools)
    --     ]
    , row [ width fill ]
        [ withTooltip "Total value stored in accounts, but not assigned to any pool" <| el [ alignLeft ] <| text <| "Unassigned"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue stakeDetail.unassigned ]
        ]
    , row [ width fill ]
        [ withTooltip "Total value staked between pools" <| el [ width fill, alignLeft ] <| text <| "Total Staked"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [] <| text <| formatAdaValue <| List.foldl (+) 0 (List.map (\pool -> pool.value) stakeDetail.pools) ]
        ]
    , row [ width fill ]
        [ withTooltip "Total value in circulation" <| el [ width fill, alignLeft ] <| text <| "Total"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [] <| text <| formatAdaValue (stakeDetail.dangling + stakeDetail.unassigned + List.foldl (+) 0 (List.map (\pool -> pool.value) stakeDetail.pools)) ]
        ]
    ]


viewStakePoolsTable : List Pool -> Bool -> Bool -> Element Msg
viewStakePoolsTable stakePoolsList showZeroStaked sortAscending =
    let
        stakePools =
            if showZeroStaked then
                stakePoolsList

            else
                List.filter (\stakePool -> stakePool.value > 0) stakePoolsList

        stakePoolsSorted =
            if sortAscending then
                List.sortBy .value stakePools

            else
                List.reverse <| List.sortBy .value stakePools
    in
    column
        [ width (fillPortion 2)
        , Font.size 14
        , onLeft
            (column [ alignRight, spacing 10, width fill, padding 5 ]
                [ Input.button toggleButtonStyle { onPress = Just ToggleShowZeroStaked, label = withTooltip "Show/Hide Empty Pools" <| toggleShowHideIcon showZeroStaked }
                , Input.button toggleButtonStyle { onPress = Just ToggleSortAscending, label = withTooltip "Sort by Value" <| toggleSortingIcon sortAscending }
                ]
            )
        ]
        [ Element.table
            [ spacing 10
            , width fill
            , scrollbarY
            , height
                (fill
                    |> maximum 860
                )
            , fontMono
            ]
            { data = stakePoolsSorted
            , columns =
                [ { header = Element.text "Pool Node ID"
                  , width = fillPortion 4
                  , view =
                        \stakePool ->
                            row [ width fill ] [ Element.text <| stakePool.id ]
                  }
                , { header = Element.text "Stake"
                  , width = fill
                  , view =
                        \stakePool ->
                            row [ spacing 5, width fill, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePool.value ]
                  }
                ]
            }
        ]


formatAdaValue : Int -> String
formatAdaValue adaValue =
    let
        adaFloat =
            toFloat adaValue
    in
    FormatNumber.format { decimals = 4, thousandSeparator = ".", decimalSeparator = ",", negativePrefix = "−", negativeSuffix = "", positivePrefix = "", positiveSuffix = "", zeroPrefix = "", zeroSuffix = "" } adaFloat


viewStakePoolsDetails : List ( String, Int ) -> List (Element Msg)
viewStakePoolsDetails stakePoolsList =
    List.map
        (\stakePool ->
            column []
                [ el [] <| text <| Tuple.first stakePool
                , el [ alignRight ] <| text <| String.fromInt <| Tuple.second stakePool
                ]
        )
        stakePoolsList


viewLeaders : List Int -> List (Element Msg)
viewLeaders leadersList =
    List.map
        (\leader ->
            el [] <| text <| String.fromInt leader
        )
        leadersList


viewLeadersLogs : List LeadersLogs -> Element Msg
viewLeadersLogs leadersLogsList =
    column [ width fill, spacing 10, height (fill |> maximum 950) ]
        [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 32 ] <| text <| "Stake Pool Leaders"
            ]
        , Element.table
            [ spacing 10
            , width fill
            , scrollbarY
            , height
                (fill
                    |> maximum 860
                )
            , fontMono
            , Font.size 14
            ]
            { data = leadersLogsList
            , columns =
                -- [ { header = Element.text "Enclave Leader ID"
                --   , width = fill
                --   , view =
                --         \leaderLog ->
                --             row [ width fill ] [ text <| String.fromInt leaderLog.enclave_leader_id ]
                --   }
                [ { header = Element.text "Created at"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ width fill ] [ text <| leaderLog.created_at_time ]
                  }
                , { header = Element.text "Scheduled at Time"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ spacing 5, width fill, alignRight ] [ text <| leaderLog.scheduled_at_time ]
                  }
                , { header = Element.text "Scheduled at Date"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ spacing 5, width fill, alignRight ] [ text <| leaderLog.scheduled_at_date ]
                  }
                , { header = Element.text "Wake at"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ spacing 5, width fill, alignRight ] [ text <| leaderLog.wake_at_time ]
                  }
                , { header = Element.text "Finished at"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ spacing 5, width fill, alignRight ] [ text <| leaderLog.finished_at_time ]
                  }
                , { header = Element.text "Status"
                  , width = fill
                  , view =
                        \leaderLog ->
                            row [ spacing 5, width fill, alignRight ]
                                [ case leaderLog.status of
                                    Pending status ->
                                        el [ alignRight ] <| text <| status

                                    Created createdBlock ->
                                        viewLeaderLogsStatusCreatedBlock createdBlock.block

                                    Rejected rejectedBlock ->
                                        el [ alignRight ] <| text <| rejectedBlock.rejected.reason
                                ]
                  }
                ]
            }
        ]


viewLeaderLogsStatusCreatedBlock : LeaderLogsCreatedBlock -> Element Msg
viewLeaderLogsStatusCreatedBlock leaderLogsStatusBlock =
    column []
        [ row [ width fill, spacing 10 ]
            [ withTooltip "Block hash that has been created" <| el [ alignLeft ] <| text <| "Block"
            , el [ alignRight ] <| text <| leaderLogsStatusBlock.block
            ]
        , row [ width fill, spacing 10 ]
            [ withTooltip "Chain length of created block" <| el [ alignLeft ] <| text <| "Chain length"
            , el [ alignRight ] <| text <| String.fromInt leaderLogsStatusBlock.chain_length
            ]
        ]


viewFragmentLogs : List FragmentLogs -> List (Element Msg)
viewFragmentLogs fragmentLogsList =
    List.map
        (\fragmentLog ->
            column []
                [ row []
                    [ el [ alignLeft ] <| text <| "Fragment ID"
                    , el [ alignRight ] <| text <| fragmentLog.fragment_id
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Received from"
                    , el [ alignRight ] <| text <| fragmentLog.received_from
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Received at"
                    , el [ alignRight ] <| text <| fragmentLog.received_at
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Last Updated at"
                    , el [ alignRight ] <| text <| fragmentLog.last_updated_at
                    ]
                , row []
                    [ el [ alignLeft ] <| text <| "Status"
                    , el [ alignRight ] <| text <| fragmentLog.status
                    ]
                ]
        )
        fragmentLogsList


viewAccount : Account -> List (Element Msg)
viewAccount account =
    [ row []
        [ el [ alignLeft ] <| text <| "Value"
        , el [ alignRight ] <| text <| String.fromInt account.value
        ]
    , row []
        [ el [ alignLeft ] <| text <| "Delegation"
        , viewPanel (viewStakePoolsSimple account.delegation.pools)
        ]
    , row []
        [ el [ alignLeft ] <| text <| "Counter"
        , el [ alignRight ] <| text <| String.fromInt account.counter
        ]
    ]


withTooltip : String -> Element msg -> Element msg
withTooltip str element =
    el
        [ Element.inFront <|
            el
                [ width fill
                , height fill
                , Element.transparent True
                , Element.mouseOver [ Element.transparent False ]
                , Element.inFront (tooltip str)
                ]
                Element.none
        ]
        element


tooltip : String -> Element msg
tooltip str =
    el
        [ Background.color (Element.rgb 0 0 0)
        , Font.color (Element.rgb 1 1 1)
        , Element.moveUp 32
        , Element.moveLeft 32
        , padding 4
        , Border.rounded 5
        , Font.size 14
        , Border.shadow
            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
        , Element.htmlAttribute (Html.Attributes.style "pointerEvents" "none")
        ]
        (text str)


formatDate : String -> String
formatDate decodedDate =
    let
        splitDate =
            String.split "T" decodedDate
    in
    case splitDate of
        x :: xs ->
            String.append x
                (case xs of
                    [ t ] ->
                        " " ++ String.left 8 t

                    _ ->
                        ""
                )

        _ ->
            ""



-- UI


mainBackgroundColor : Element.Color
mainBackgroundColor =
    Element.rgb255 239 239 239


sidebarBackgroundColor : Element.Color
sidebarBackgroundColor =
    Element.rgb255 70 80 88


mainMessageColor : Element.Color
mainMessageColor =
    Element.rgb255 94 96 102


borderedBoxBackgroundColor : Element.Color
borderedBoxBackgroundColor =
    Element.rgb255 250 251 252


borderedBoxBorder : List (Element.Attr () msg)
borderedBoxBorder =
    [ Border.solid, Border.color (Element.rgb255 198 205 214), Border.width 1 ]


borderedBoxTextColor : Element.Color
borderedBoxTextColor =
    Element.rgb255 94 96 102


fontNormal : Element.Attribute msg
fontNormal =
    Font.family
        [ Font.external
            { name = "Open Sans"
            , url = "https://fonts.googleapis.com/css?family=Open+Sans"
            }
        , Font.sansSerif
        ]


fontMono : Element.Attribute msg
fontMono =
    Font.family
        [ Font.external
            { name = "IBM Plex Mono"
            , url = "https://fonts.googleapis.com/css?family=IBM+Plex+Mono"
            }
        , Font.monospace
        ]


linkColor : Element.Color
linkColor =
    Element.rgb 33 120 180


blue : Element.Color
blue =
    Element.rgb255 41 106 157


grey : Element.Color
grey =
    Element.rgb255 183 185 181


buttonStyle : List (Element.Attr () msg)
buttonStyle =
    [ Background.color blue
    , Font.center
    , fontNormal
    , Element.focused
        [ Background.color grey ]
    , Border.width 2
    , Border.rounded 2
    , width <|
        (fill
            |> Element.maximum 150
            |> Element.minimum 30
        )
    , Element.height <|
        (fill
            |> Element.maximum 150
            |> Element.minimum 30
        )
    ]


toggleButtonStyle : List (Element.Attr () msg)
toggleButtonStyle =
    [ alignRight
    ]



-- SVG


aboutSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 28 28" ] [ Svg.path [ SvgA.style "fill: currentColor", SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M14 28C6.268 28 0 21.732 0 14S6.268 0 14 0s14 6.268 14 14-6.268 14-14 14zM8.742 11.01h3.174c.03-.947.674-1.572 1.611-1.572.918 0 1.563.546 1.563 1.337 0 .86-.342 1.27-1.67 2.051-1.357.781-1.846 1.66-1.66 3.33l.03.39h3.046v-.458c0-.918.371-1.358 1.709-2.129 1.435-.83 2.11-1.826 2.11-3.271 0-2.344-1.934-3.907-4.903-3.907-3.125 0-4.98 1.67-5.01 4.229zM13.4 21.342c1.26 0 1.993-.654 1.993-1.778 0-1.132-.733-1.787-1.993-1.787-1.26 0-2.002.655-2.002 1.787 0 1.123.743 1.778 2.002 1.778z" ] [] ]


adaSvg =
    svg [ SvgA.width "12", SvgA.height "12", SvgA.viewBox "0 0 18 20", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd", SvgA.strokeLinecap "round" ] [ g [ SvgA.id "new_wallet_screen_summary", SvgA.transform "translate(-1238.000000, -715.000000)", SvgA.stroke "#000", SvgA.strokeWidth "1.5" ] [ g [ SvgA.id "amount-copy", SvgA.transform "translate(760.000000, 460.000000)" ] [ g [ SvgA.id "pending", SvgA.transform "translate(60.000000, 246.000000)" ] [ g [ SvgA.id "ada-symbol-smallest-dark-copy", SvgA.transform "translate(419.000000, 10.000000)" ] [ polyline [ SvgA.id "Path-2", SvgA.strokeLinejoin "round", SvgA.points "0.505531915 18 7.92 0 15.3344681 18" ] [], Svg.path [ SvgA.d "M1.68510638,7.75 L14.1548936,7.75", SvgA.id "Line" ] [], Svg.path [ SvgA.d "M0,10.75 L15.84,10.75", SvgA.id "Line-Copy" ] [] ] ] ] ] ] ]


sortAscSvg =
    svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 14 15", SvgA.version "1.1", SvgA.id "svg4" ] [ Svg.metadata [ SvgA.id "metadata10" ] [], defs [ SvgA.id "defs8" ] [], Svg.path [ SvgA.d "M 0,15 H 14 V 12 H 0 Z M 0,9 H 10 V 6 H 0 Z M 0,3 H 6 V 0 H 0 Z", SvgA.id "path2", SvgA.style "SvgA.fill:#5e6066;SvgA.fill-rule:evenodd" ] [] ]


sortDscSvg =
    svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 14 15" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M0 0h14v3H0V0zm0 6h10v3H0V6zm0 6h6v3H0v-3z" ] [] ]


errorerSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 24 24" ] [ Svg.path [ SvgA.fill "#EA4C5B", SvgA.d "M12 24C5.373 24 0 18.627 0 12S5.373 0 12 0s12 5.373 12 12-5.373 12-12 12zM10.202 4.499l.28 10.398h2.868L13.64 4.5h-3.438zm-.236 13.997c0 1.128.752 1.826 1.955 1.826 1.214 0 1.955-.698 1.955-1.826 0-1.139-.741-1.837-1.955-1.837-1.203 0-1.955.698-1.955 1.837z" ] [] ]


fetchDataSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 130 116", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "3", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "fetch_data", SvgA.transform "translate(-1885.000000, -1022.000000)" ] [ g [ SvgA.id "dialog", SvgA.transform "translate(1000.000000, 355.000000)" ] [ g [ SvgA.id "fetch", SvgA.transform "translate(660.000000, 570.000000)" ] [ g [ SvgA.id "fetch-ic", SvgA.transform "translate(196.000000, 61.000000)" ] [ circle [ SvgA.id "Oval", SvgA.cx "94", SvgA.cy "94", SvgA.r "94" ] [], Svg.path [ SvgA.d "M132,68 C130.341,68 129,69.341 129,71 C129,72.659 130.341,74 132,74 L144,74 C148.968,74 153,78.026 153,83 L153,137 C153,141.968 148.968,146 144,146 L44,146 C39.032,146 35,141.968 35,137 L35,83 C35,78.026 39.032,74 44,74 L56,74 C57.659,74 59,72.659 59,71 C59,69.341 57.659,68 56,68 L44,68 C35.714,68 29,74.714 29,83 L29,137 C29,145.286 35.714,152 44,152 L144,152 C152.286,152 159,145.286 159,137 L159,83 C159,74.714 152.286,68 144,68 L132,68 Z M91,39 L91,115.857 L71.998,96.855 C70.855,95.712 68.998,95.712 67.855,96.855 C66.712,98.004 66.712,99.855 67.855,100.998 L91.579,124.728 C91.678,124.863 91.732,125.022 91.855,125.145 C92.437,125.73 93.205,126.006 93.97,125.994 C94.792,126.006 95.56,125.73 96.145,125.145 C96.271,125.016 96.331,124.857 96.43,124.716 L120.145,100.998 C121.288,99.855 121.288,98.004 120.145,96.855 C118.999,95.712 117.145,95.712 116.002,96.855 L97,115.857 L97,39 C97,37.344 95.659,36 94,36 C92.341,36 91,37.341 91,39 Z", SvgA.id "fetch", SvgA.fill "#5E6066", SvgA.style "fill: currentColor" ] [] ] ] ] ] ] ]


hideSvg =
    svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 38 38" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M11.708 27.555l-6.143 6.143-1.414-1.414 5.572-5.572C6.29 25.055 3.049 22.434 0 18.85c5.671-6.666 12.004-10 19-10 2.516 0 4.947.432 7.292 1.294L32.435 4l1.414 1.414-5.572 5.572c3.433 1.657 6.674 4.278 9.723 7.863-5.671 6.667-12.004 10-19 10-2.516 0-4.947-.431-7.292-1.294zm1.512-1.511c1.867.537 3.794.805 5.78.805 6.26 0 11.926-2.666 17-8-2.853-2.998-5.892-5.153-9.12-6.466l-3.708 3.709a5 5 0 0 1-6.929 6.929l-3.023 3.023zm-2.1-.728l3.708-3.71a5 5 0 0 1 6.929-6.929l3.023-3.022a20.802 20.802 0 0 0-5.78-.806c-6.26 0-11.926 2.667-17 8 2.853 2.998 5.892 5.154 9.12 6.467z" ] [] ]


loadedSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 220 220", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "wallet_add_wallet_notification_big", SvgA.transform "translate(-1526.000000, -734.000000)" ] [ g [ SvgA.id "notification", SvgA.transform "translate(720.000000, 168.000000)" ] [ g [ SvgA.id "description", SvgA.transform "translate(660.000000, 566.000000)" ] [ g [ SvgA.id "checked-light", SvgA.transform "translate(146.000000, 0.000000)" ] [ Svg.path [ SvgA.d "M110,220 C49.2486775,220 0,170.751322 0,110 C0,49.2486775 49.2486775,0 110,0 C170.751322,0 220,49.2486775 220,110 C220,170.751322 170.751322,220 110,220 Z M110,205 C162.467051,205 205,162.467051 205,110 C205,57.5329488 162.467051,15 110,15 C57.5329488,15 15,57.5329488 15,110 C15,162.467051 57.5329488,205 110,205 Z", SvgA.id "Combined-Shape", SvgA.fill "#05f079" ] [], Svg.path [ SvgA.d "M90,110 L145,110 L145,125 L75,125 L75,117.5 L75,85 L90,85 L90,110 Z", SvgA.id "Combined-Shape", SvgA.fill "#05f079", SvgA.transform "translate(110.000000, 105.000000) rotate(-45.000000) translate(-110.000000, -105.000000) " ] [] ] ] ] ] ] ]


leadersSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 24 24" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M11.219,3.375L8,7.399L4.781,3.375C4.515,3.043,4.068,2.916,3.669,3.056C3.269,3.197,3,3.575,3,4v15c0,1.103,0.897,2,2,2 h14c1.103,0,2-0.897,2-2V4c0-0.425-0.269-0.803-0.669-0.944c-0.4-0.138-0.846-0.012-1.112,0.319L16,7.399l-3.219-4.024 C12.4,2.901,11.6,2.901,11.219,3.375z M5,19v-2h14.001v2H5z M15.219,9.625c0.381,0.475,1.182,0.475,1.563,0L19,6.851L19.001,15H5 V6.851l2.219,2.774c0.381,0.475,1.182,0.475,1.563,0L12,5.601L15.219,9.625z" ] [] ]


loadingSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 122 122", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "styles", SvgA.transform "translate(-860.000000, -5380.000000)", SvgA.fill "#FAFBFC" ] [ g [ SvgA.id "spinner-light", SvgA.transform "translate(860.000000, 5380.000000)" ] [ Svg.path [ SvgA.d "M18.5735931,18.5735931 C-4.85786437,42.0050506 -4.85786437,79.9949494 18.5735931,103.426407 C42.0050506,126.857864 79.9949494,126.857864 103.426407,103.426407 L97.7695526,97.7695526 C77.4622895,118.076816 44.5377105,118.076816 24.2304474,97.7695526 C3.92318421,77.4622895 3.92318421,44.5377105 24.2304474,24.2304474 L18.5735931,18.5735931 L18.5735931,18.5735931 L18.5735931,18.5735931 Z", SvgA.id "spinner" ] [] ] ] ] ]


monitorSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 44 44" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.fillRule "evenodd", SvgA.d "M38.737 13C35.53 7.046 29.237 3 22 3 11.507 3 3 11.507 3 22H0C0 9.85 9.85 0 22 0c8.103 0 15.182 4.38 19 10.902V7h3v9h-9v-3h3.737zM3.967 28H9v3H5.263C8.47 36.954 14.763 41 22 41c10.493 0 19-8.507 19-19h3c0 12.15-9.85 22-22 22-8.103 0-15.182-4.38-19-10.902V37H0v-9H3.967z" ] [] ]


nodeSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 62 58" ] [ g [ SvgA.fill "none", SvgA.fillRule "nonzero" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M10.75 0C16.687 0 21.5 4.813 21.5 10.75S16.687 21.5 10.75 21.5 0 16.687 0 10.75 4.813 0 10.75 0zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM10.75 36c5.937 0 10.75 4.813 10.75 10.75S16.687 57.5 10.75 57.5 0 52.687 0 46.75 4.813 36 10.75 36zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM50.75 0C56.687 0 61.5 4.813 61.5 10.75S56.687 21.5 50.75 21.5 40 16.687 40 10.75 44.813 0 50.75 0zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM50.75 36c5.937 0 10.75 4.813 10.75 10.75S56.687 57.5 50.75 57.5 40 52.687 40 46.75 44.813 36 50.75 36zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M30.75 0C46.628 0 59.5 12.872 59.5 28.75S46.628 57.5 30.75 57.5 2 44.628 2 28.75 14.872 0 30.75 0zm0 2.5C16.253 2.5 4.5 14.253 4.5 28.75S16.253 55 30.75 55 57 43.247 57 28.75 45.247 2.5 30.75 2.5z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M10.75 36c2.969 0 5.657 1.204 7.602 3.15l-1.767 1.767A8.25 8.25 0 1 0 4.918 52.584L3.15 54.353A10.716 10.716 0 0 1 0 46.75C0 40.813 4.813 36 10.75 36zM10.75 0C16.687 0 21.5 4.813 21.5 10.75c0 2.969-1.203 5.656-3.149 7.601l-1.767-1.767A8.25 8.25 0 1 0 4.917 4.917L3.148 3.148A10.716 10.716 0 0 1 10.75 0zM61.5 10.75c0 5.937-4.813 10.75-10.75 10.75a10.716 10.716 0 0 1-7.601-3.149l1.768-1.766A8.25 8.25 0 0 0 56.584 4.918l1.767-1.77A10.716 10.716 0 0 1 61.5 10.75zM43.149 39.149l1.767 1.767a8.25 8.25 0 1 0 11.667 11.667l1.768 1.768A10.716 10.716 0 0 1 50.75 57.5C44.813 57.5 40 52.687 40 46.75c0-2.969 1.203-5.656 3.149-7.601z" ] [] ] ]


settingsSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 42 44", SvgA.style "fill: currentColor" ] [ g [ SvgA.fill "none", SvgA.fillRule "evenodd", SvgA.transform "translate(-23 -22)" ] [ circle [ SvgA.cx "44", SvgA.cy "44", SvgA.r "44" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M55 46H33c-5.521 0-10 4.477-10 10 0 5.521 4.479 10 10 10h22c5.523 0 10-4.479 10-10 0-5.524-4.477-10-10-10zm0 18H33a8 8 0 0 1 0-16h22a8 8 0 0 1 0 16zm0-42H33c-5.521 0-10 4.477-10 10 0 5.521 4.479 10 10 10h22c5.523 0 10-4.479 10-10 0-5.524-4.477-10-10-10zm0 18H33a8 8 0 0 1 0-16h22a8 8 0 0 1 0 16zm0 10a6 6 0 0 0 0 12 6 6 0 0 0 0-12zm0 10a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM33 26a6 6 0 0 0 0 12 6 6 0 0 0 0-12zm0 10a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" ] [] ] ]


showSvg =
    svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 38 38" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M19 29c-6.996 0-13.329-3.333-19-10C5.671 12.333 12.004 9 19 9s13.329 3.333 19 10c-5.671 6.667-12.004 10-19 10zm0-2c6.26 0 11.926-2.667 17-8-5.074-5.333-10.74-8-17-8S7.074 13.667 2 19c5.074 5.333 10.74 8 17 8zm0-3a5 5 0 1 1 0-10 5 5 0 0 1 0 10z" ] [] ]


stakePoolsSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 44 36", SvgA.version "1.1" ] [ g [ SvgA.id "Symbols", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "small-menu-staking-selected", SvgA.transform "translate(-62.000000, -906.000000)" ] [ g [ SvgA.id "menu" ] [ g [ SvgA.id "main-menu", SvgA.transform "translate(40.000000, 208.000000)" ] [ g [ SvgA.id "staking-ic", SvgA.transform "translate(0.000000, 672.000000)" ] [ circle [ SvgA.id "Oval", SvgA.cx "44", SvgA.cy "44", SvgA.r "44" ] [], polygon [ SvgA.id "Fill-164", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "34 60 34 40 28 40 28 60 26 60 26 38 36 38 36 60" ] [], polygon [ SvgA.id "Fill-165", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "47 60 47 34 41 34 41 60 39 60 39 32 49 32 49 60" ] [], polygon [ SvgA.id "Fill-166", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "60 60 60 28 54 28 54 60 52 60 52 26 62 26 62 60" ] [], polygon [ SvgA.id "Fill-167", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "22 60 66 60 66 62 22 62" ] [] ] ] ] ] ] ]


statusSvg =
    svg [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 30 30" ] [ g [ SvgA.fill "#EE2257", SvgA.fillRule "nonzero" ] [ Svg.path [ SvgA.d "M19.353 25.044a.937.937 0 0 1-1.828-.076L14.711 9.423 11.7 22.584a.937.937 0 0 1-1.817.042l-1.986-7.13-.18.54a.938.938 0 0 1-.89.642H.094C.928 24.172 7.283 30 15 30c7.717 0 14.072-5.828 14.906-13.322h-8.312l-2.24 8.366z" ] [], Svg.path [ SvgA.d "M7.087 11.99a.938.938 0 0 1 1.793.044l1.817 6.526 3.207-14.017a.937.937 0 0 1 1.837.042l2.884 15.931 1.344-5.018a.937.937 0 0 1 .906-.695h9.123C29.892 6.61 23.218 0 15 0S.108 6.61.002 14.803h6.149l.936-2.813z" ] [] ] ]


anchorSvg =
    svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 23 14", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "wallet_add_wallet_create_personal", SvgA.transform "translate(-2157.000000, -916.000000)", SvgA.fill "#5E6066" ] [ g [ SvgA.id "dialog", SvgA.transform "translate(1000.000000, 331.000000)" ] [ g [ SvgA.id "currency", SvgA.transform "translate(60.000000, 478.000000)" ] [ g [ SvgA.id "input", SvgA.transform "translate(0.000000, 64.000000)" ] [ polygon [ SvgA.id "expand-arrow", SvgA.points "1108.5 51.4 1099.875 43 1097 45.8 1107.0625 55.6 1108.5 57 1120 45.8 1117.125 43" ] [] ] ] ] ] ] ]


oxheadSvg =
    svg [ SvgA.width "60", SvgA.height "60", SvgA.viewBox "0 0 324 323", SvgA.fill "none" ] [ g [ SvgA.clipPath "url(#clip0)" ] [ Svg.path [ SvgA.d "M88 223.346L158.375 152.972V1.52588e-05L88 70.375L88 223.346Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M171.375 1.52588e-05L241.75 70.374V223.346L171.375 152.971V1.52588e-05Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M240.998 241.138L164.86 165L88.375 241.485L164.513 317.623L240.998 241.138Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M248.952 255L316.729 322.776H181.176L248.952 255Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M257.728 243.865L327.594 174V313.731L257.728 243.865Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M78.777 255L11 322.776H146.553L78.777 255Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [], Svg.path [ SvgA.d "M69.866 243.865L0 174L0 313.731L69.866 243.865Z", SvgA.fill "white", SvgA.style "fill: currentColor" ] [] ], defs [] [ Svg.clipPath [ SvgA.id "clip0" ] [ rect [ SvgA.width "323.141", SvgA.height "322.95", SvgA.fill "white", SvgA.style "fill: currentColor" ] [] ] ] ]



-- Main


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
