# Ox Head

Intents to be a lightweight dashboard and control system for your Cardano node.

## Mitology

Thor catch Jörmungandr by preparing a strong line and a large hook which baits it with an **ox head**.

## Requirements

* Running Jörmungandr Node
* Enable CORS for this domain in node config

## Build guid 

### Prerequisits 

* elm
* docker
* docker-compose 

### Build steps

 pull the code

```sh
git clone https://gitlab.com/arguser/oxhead.git
```

* navigate to the oxhead/src folder

```sh
cd oxhead/src 
```

* run the build

```sh
elm make OxHead.elm
```

* deploy using docker-compose

```sh
cd ..
docker-compose up -d 
```

The Oxhead deployment is now available on the IP of the mashine you are running on just type the IP in any browser(chrome,firefox etc.)

Note: You can now make any changes to the code and rebuild, the changes will automaticaly update when you refresh the browser

* To stop the application: 

```sh
docker-compose down 
```

## Development

Suggestions are welcome at the [Issue Board](https://gitlab.com/arguser/oxhead/-/boards)
